# Keycount
A command line utility that counts characters, words, pages, and specific word occurrences in one or more text files.

---
---
## Usage
`keycount` have to be called with the names of the files as positional arguments. The output will display the number of words, characters ad pages for each file. If more than one file name is passed as argument there will be a section at the bottom of the output with the total sum of characters, words and pages of all the files.
One page is 1800 characters by default.

### Positional arguments:
  * `<file_names>`           the names of the text file(s) to read

### Optional arguments:
  * `-h`, `--help`           show the help message and exit
  * `-p`, `--page`          the number of characters in a page (default 1800)
  * `-o`, `--occurrences`   use this option to count the occurrences of a specified word
  * `-c`, `--case`           if present this option will make the word occurrences counter case sensitive
